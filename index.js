const fetch = require('node-fetch');
const http = require('http');
const https = require('https');

const validator = async (config, res, content) =>{
  // run validation code
  try {
    const validFunc = new Function('context', config.validator.func);

    return  validFunc({
        content,
        res,
        config,
      });
  } catch(e) {
    return {
      errorInternal: 'Error running validator: ' + e.toString(),
    };
  }
}

const confReq = (config, loggable) => {

  const options = {};

  if(config.agent && !loggable){
    const agent = (_parsedURL) => {
      if (_parsedURL.protocol == 'http:') {
          return new http.Agent(config.agent);
      } else {
          return new https.Agent(config.agent);
      }
    }

    options.agent = agent;
  }else if (config.agent && loggable){
    options.agent = config.agent;
  }

  if(config.headers){
    options.headers = config.headers;
  }

  if(config.body){
    options.body = config.body;
  }

  if(config.method){
    options.method = config.method;
  }

  if(config.timeout){
    options.timeout = config.timeout;
  }

  return options;
}

module.exports = {
  name: 'Simple Validator',
  config: {
    type: 'simple',
  },
  validate: () => { return true; },
  run: async (config, url = null) => {
    try{
      const startTime = new Date();
      const fetchOptions = confReq(config);
      const fetchOptionsLoggable = confReq(config, true);
      const errorReport = {
        url: url || config.url,
        req: fetchOptionsLoggable,
      };

      let res = null;
      try{
        res = await fetch(url || config.url, fetchOptions);
      }catch(e){
        console.warn('💥 SimpleRequest: fetch error: ', e);
        if(e.message){
          errorReport.message = e.toString();
        }
        if(e.toString){
          errorReport.exception = e.toString();
        }
        return  {
          timing: new Date() - startTime.getTime(),
          success: false,
          gotResponse: false,
          slow: false,
          error: true,
          errorReport: errorReport,
        };
      }
      const endTime = new Date();
      const timing = endTime.getTime() - startTime.getTime();
      if(!res.ok){
        errorReport.response = await res.text();
        errorReport.responseHeaders = await res.headers.raw();
        errorReport.status = res.status;
      }

      let validated = null;
      if(config.validator && res.ok){

        let content = await res.text();
        let validResp = null;
        if(config.validator.json){
          try {
            jsonContent = JSON.parse(content);
            validResp = await validator(config, res, jsonContent);
          }catch(e){
            validResp = { errorInternal: 'Error converting to json: ' + e.toString() };
          }
        }else{
          validResp = await validator(config, res, content);
        }

        if(validResp && validResp.errorInternal){ // internal error
          validated = false;
          errorReport.response = content;
          errorReport.responseHeaders = await res.headers.raw();
          errorReport.status = res.status;
          errorReport.message = validResp.errorInternal;
        }else if(validResp !== true){
          errorReport.response = content;
          errorReport.responseHeaders = await res.headers.raw();
          errorReport.status = res.status;
          errorReport.validator = validResp;
          validated = false;
        }else{
          validated = true;
        }
      }

      const details = {
        timing: timing,
        success: res.ok && validated !== false,
        validated: validated,
        gotResponse: res.status !== 0,
        slow: config.slow ? timing > config.slow: false,
        errorReport: res.ok && validated !== false ? null : errorReport,
      };
      return details;
    }catch(e){
      console.error('💥 SimpleRequest: Errors running request: ', e);
      return null;
    }
  },
};
